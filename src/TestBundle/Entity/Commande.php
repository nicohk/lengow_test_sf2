<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 */
class Commande
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orderId;

    /**
     * @var string
     */
    private $orderStatus;

    /**
     * @var string
     */
    private $orderShipping;

    /**
     * @var \DateTime
     */
    private $paymentDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     * @return Commande
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderStatus
     *
     * @param string $orderStatus
     * @return Commande
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return string 
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set orderShipping
     *
     * @param string $orderShipping
     * @return Commande
     */
    public function setOrderShipping($orderShipping)
    {
        $this->orderShipping = $orderShipping;

        return $this;
    }

    /**
     * Get orderShipping
     *
     * @return string 
     */
    public function getOrderShipping()
    {
        return $this->orderShipping;
    }

    /**
     * Set paymentDate
     *
     * @param \DateTime $paymentDate
     * @return Commande
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * Get paymentDate
     *
     * @return \DateTime 
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }
}
