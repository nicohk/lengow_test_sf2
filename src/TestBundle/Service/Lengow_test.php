<?php
/**
 * Created by PhpStorm.
 * User: PC Dell
 * Date: 24/04/2017
 * Time: 23:41
 */

namespace TestBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use TestBundle\Entity\Commande;


class Lengow_test
{

    private $logger;
    private $urlOrders;

    public function __construct(LoggerInterface $logger, $urlOrders, $em)
    {
        $this->logger = $logger;
        $this->urlOrders = $urlOrders;
        $this->em = $em;
    }

    public function getLengowXml()
    {

        $crawler = new Crawler();
        $xml = file_get_contents($this->urlOrders);

        $this->logger->info('appels getLengowXml');

        $crawler->add($xml);

        $orders = $crawler->filter('order')->each(
            function (Crawler $nodeCrawler) {
            $orderId = $nodeCrawler->filter('order_id');
            $orderStatus = $nodeCrawler->filter('order_status');
            $orderShipping = $nodeCrawler->filter('order_shipping');
            $paymentDate = $nodeCrawler->filter('payment_date');

            $commande = new Commande();
            $commande->setOrderId($orderId->text());
            $commande->setOrderStatus($orderStatus->text());
            $commande->setOrderShipping($orderShipping->text());
            $commande->setPaymentDate(new \DateTime($paymentDate->text()));

            return $commande;
        });

        $this->logger->info('return getLengowXml');

        foreach ($orders as $commande){
            $orderExist = $this->em->getRepository('TestBundle:Commande')->findOneBy(array('orderId' => $commande->getOrderId()));
            if (!isset($orderExist)) {
                $this->em->persist($commande);
            }
        }

        $this->em->flush();

    }
}